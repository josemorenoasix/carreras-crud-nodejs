const createError = require('http-errors');
const express = require('express');
const session = require('express-session');
const fileUpload = require('express-fileupload');
const flash = require('connect-flash');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

// Mongoose Connection
const mongoose = require('mongoose');
mongoose.set('debug', true);
const uri = 'mongodb://localhost/dbCarreras';
const options = {
    useNewUrlParser: true,
    reconnectTries: 60,
    reconnectInterval: 1000
};
mongoose.connect(uri, options)
    .then(() => {
        console.log('Database connection successful');
    })
    .catch(err => {
        console.error('Database connection error')
    });

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const carrerasRouter = require('./routes/carreras');
const participantesRouter = require('./routes/participantes');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser('secretToken'));
app.use(session({
        secret: 'secretToken',
        cookie: {maxAge: 64000},
        resave: true,
        saveUninitialized: true,
    })
);
app.use(flash());
app.use(fileUpload());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/carreras', carrerasRouter);
app.use('/participantes', participantesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
