const express = require('express');
const router = express.Router();

const Participante = require('../models/participante');
const Carrera = require('../models/carrera');
const Dorsal = require('../models/dorsal');
router.get('/', function(req, res, next) {
    try {
        Participante.find()
            .then(participantes => {
                console.log(participantes);
                res.render('participantes/listado', { participantes: participantes, mensaje: req.flash('mensaje') });
            })
            .catch(err => {
                console.error(err);
                res.render('participantes/listado', { mensaje: err });
            });
    } catch (exception) {
        console.log(exception);
        res.render('participantes/listado', { mensaje: exception });
    }
});

router.get('/:id/detalle', function(req, res, next) {
    try {
        Participante.findById(req.params.id)
            .populate('carrera', ['numero', 'descripcion', 'fecha', 'distancia'])
            .populate('dorsal', ['numero', 'codigoChip'])
            .then(participante => {
                console.log(participante);
                res.render('participantes/detalle', { participante: participante, mensaje: req.flash('mensaje') });
            })
            .catch(err => {
                console.error(err);
                res.render('participantes/listado', { mensaje: err });
            });
    } catch (exception) {
        console.log(exception);
        res.render('participantes/listado', { mensaje: exception });
    }
});

router.get('/:id/editar', function(req, res, next) {
    try {
        Participante.findById(req.params.id)
            .then(result => {
                console.log(result);
                res.render("participantes/editarForm", {participante: result, mensaje: req.flash('mensaje')});
            })
            .catch(err => {
                console.log(err);
                req.flash('mensaje', err.sqlMessage);
                res.redirect('/participantes');
            })
    } catch (exception) {
        console.log(exception);
        req.flash('mensaje', exception.toString());
        res.redirect('/participantes');
    }
});

router.post('/:id/editar', function(req, res, next) {
    try {
        const data = {
            nombre: req.body.nombre,
            apellidos: req.body.apellidos,
            club: req.body.club,
            dni: req.body.dni
        };
        const participante = new Participante(data);
        participante.validate()
            .then(() => {
                Participante.findOneAndUpdate({_id: req.params.id}, data)
                    .then(p => {
                        console.log(p);
                        req.flash('mensaje', `El participante con id ${p._id} ha sido actualizado`);
                        res.redirect(`/carreras/${p.carrera}/participantes`);
                    })
                    .catch(err => {
                        console.log(err);
                        req.flash('mensaje', err);
                        res.redirect('/participantes/crear');
                    })
            })
            .catch(err => {
                console.log(err);
                req.flash('mensaje', err.message);
                res.redirect('/participantes/crear');
            })
    } catch (exception) {
        console.log(exception);
        req.flash('mensaje', exception);
        res.redirect('/participantes/crear');
    }
});

router.get('/:id/eliminar', function(req, res, next) {
    try {
        Participante.findByIdAndDelete(req.params.id)
            .then(participante => {
                console.log(participante);

                Dorsal.findByIdAndDelete(participante.dorsal._id)
                    .then(dorsal => {
                        console.log(dorsal);
                    })
                    .catch(err => {
                    console.log(err);
                });
                Carrera.findByIdAndUpdate(participante.carrera, {$pull: {participantes: participante._id}})
                    .then(carrera => {
                        console.log(carrera);
                    })
                    .catch(err => {
                        console.log(err);
                    });
                req.flash('mensaje',
                    `El participante con id ${participante._id} ha sido eliminado`);
                res.redirect('/carreras');
            })
            .catch(err => {
                console.log(err);
                req.flash('mensaje', err.toString());
                res.redirect('/carreras');
            });
    } catch (exception) {
        console.log(exception);
        req.flash('mensaje', exception.toString());
        res.redirect('/carreras');
    }

});

module.exports = router;
