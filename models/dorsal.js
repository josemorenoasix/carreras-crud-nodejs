const mongoose = require('mongoose');

const DorsalSchema = mongoose.Schema ({
    numero: {
        type: Number,
        required: true,
        unique: false
    },
    codigoChip: {
        type: String,
        required: false,
        trim: true
    }
}, {
    timestamps: true
});

// Export the model
module.exports = mongoose.model('Dorsal', DorsalSchema, 'dorsales');