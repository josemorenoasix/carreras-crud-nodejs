const express = require('express');
const router = express.Router();

const Carrera = require('../models/carrera');
const Participante = require('../models/participante' + '');
const Dorsal = require('../models/dorsal');
const mongoose = require('mongoose');
const moment = require('moment');

/* GET home page. */
router.get('/', function (req, res, next) {
    try {
        Carrera.find()
            .then(carreras => {
                res.render('carreras/listado', {carreras: carreras, mensaje: req.flash('mensaje')});
            })
            .catch(err => {
                console.error(err);
                res.render('carreras/listado', {mensaje: err});
            });
    } catch (exception) {
        console.log(exception);
        res.render('carreras/listado', {mensaje: exception});
    }
});

router.get('/:id/detalle', function (req, res, next) {
    try {
        Carrera.findById(req.params.id)
            .then(carrera => {
                console.log(carrera);
                res.render('carreras/detalle', {carrera: carrera, mensaje: req.flash('mensaje')});

            })
            .catch(err => {
                console.error(err);
                res.render('carreras/listado', {mensaje: err});
            })
    } catch (e) {
        console.exception(e);
        res.render('carreras/listado', {mensaje: e});
    }
});

router.get('/:id/participantes', function (req, res, next) {
    try {
        Carrera.findById(req.params.id)
            .populate({
                path: 'participantes',
                select: ['dorsal', 'nombre', 'apellidos', 'dni', 'club', 'tiempo', 'posicion'],
                populate: {
                    path: 'dorsal'
                }
            })
            .then(carrera => {
                //console.log(carrera);

                res.render('carreras/participantes', {
                    carrera: carrera,
                    mensaje: req.flash('mensaje'),
                    registro: req.flash('registro')[0],
                });

            })
            .catch(err => {
                console.error(err);
                res.render('carreras/listado', {mensaje: err});
            })
    } catch (e) {
        console.exception(e);
        res.render('carreras/listado', {mensaje: e});
    }
});

router.get('/:id/participantes/buscar/', function (req, res, next) {
    const rgx = new RegExp(req.query.apellidos);
    try {
        Carrera.findById(req.params.id)
            .populate({
                path: 'participantes',
                match: { apellidos: { $regex: rgx }  },
                select: ['dorsal', 'nombre', 'apellidos', 'dni', 'club', 'tiempo', 'posicion'],
                populate: {
                    path: 'dorsal'
                }
            })
            .then(carrera => {
                console.log(carrera);

                res.render('carreras/participantesBusqueda', {
                    carrera: carrera,
                    mensaje: `${carrera.participantes.length} participantes encontrados`,
                });

            })
            .catch(err => {
                console.error(err);
                res.render('carreras/listado', {mensaje: err});
            })
    } catch (e) {
        console.exception(e);
        res.render('carreras/listado', {mensaje: e});
    }
});

router.post('/:id/participantes/crear', function (req, res, next) {

    try {
        Carrera.findById(req.params.id)
            .then(carrera => {
                console.log('Carrera encontrada');
                console.log(carrera._id);
                const dorsal = new Dorsal({
                    numero: carrera.participantes.length + 1
                });
                dorsal.validate()
                    .then(() => {
                        dorsal.save()
                            .then(dorsal => {
                                console.log('Dorsal creado');
                                console.log(dorsal);

                                const participante = new Participante({
                                    dorsal: dorsal._id,
                                    nombre: req.body.nombre,
                                    apellidos: req.body.apellidos,
                                    club: req.body.club,
                                    dni: req.body.dni,
                                    carrera: carrera._id
                                });
                                participante.validate()
                                    .then(() => {
                                        participante.save()
                                            .then(participante => {
                                                console.log('Participante creado');
                                                console.log(participante);

                                                carrera.participantes.push(participante._id);
                                                carrera.save()
                                                    .then(c => {
                                                        console.log('Carrera actualizada');
                                                        console.log(c);
                                                        console.log('Tarea finalizada');
                                                        console.log(`Al participante ${participante.nombre} se le ha asignado el dorsal ${dorsal.numero}`);
                                                        console.log(c);

                                                        req.flash('mensaje', `El participante con id ${participante._id} y dorsal ${dorsal.numero}  ha sido creado`);
                                                        res.redirect(`/carreras/${req.params.id}/participantes`);
                                                    })
                                                    .catch(err => {
                                                        console.error(err);
                                                        res.render('carreras/listado', {mensaje: err})
                                                    })
                                            })
                                            .catch(err => {
                                                console.error(err);
                                                res.render('carreras/listado', {mensaje: err})
                                            })
                                    })
                                    .catch(err => {
                                        console.error(err);
                                        res.render('carreras/listado', {mensaje: err})
                                    })
                            })
                            .catch(err => {
                                console.error(err);
                                res.render('carreras/listado', {mensaje: err})
                            });
                    })
                    .catch(err => {
                        console.error(err);
                        res.render('carreras/listado', {mensaje: err})
                    });
            })
            .catch(err => {
                console.error(err);
                res.render('carreras/listado', {mensaje: err});
            })
    } catch (e) {
        console.exception(e);
        res.render('carreras/listado', {mensaje: e});
    }

});

router.get('/:id/participantes/crear', function (req, res, next) {
    res.render("participantes/crearForm", {mensaje: req.flash('mensaje')});
});

router.get('/crear', function (req, res, next) {
    res.render("carreras/crearForm", {mensaje: req.flash('mensaje')});
});

router.post('/crear', function (req, res, next) {
    try {
        const carrera = new Carrera({
            numero: req.body.numero,
            descripcion: req.body.descripcion,
            fecha: `${req.body.fecha} ${req.body.horaInicio}`,
            distancia: req.body.distancia,
        });
        carrera.validate()
            .then(() => {
                carrera.save()
                    .then(c => {
                        console.log(c);
                        req.flash('mensaje', `La carrera con id ${c._id} ha sido creada`);
                        res.redirect('/carreras');

                    })
                    .catch(err => {
                        console.error(err);
                        req.flash('mensaje', err);
                        res.redirect('/carreras/crear');
                    })
            })
            .catch(err => {
                console.error(err);
                req.flash('mensaje', err.message);
                res.redirect('/carreras/crear');
            })
    } catch (exception) {
        console.exception(exception);
        req.flash('mensaje', exception);
        res.redirect('/carreras/crear');
    }
});

router.get('/:id/editar', function (req, res, next) {
    try {
        Carrera.findById(req.params.id)
            .then(result => {
                console.log(result);
                res.render("carreras/editarForm", {carrera: result, mensaje: req.flash('mensaje')});
            })
            .catch(err => {
                console.error(err);
                req.flash('mensaje', err.sqlMessage);
                res.redirect('/carreras');
            })
    } catch (exception) {
        console.exception(exception);
        req.flash('mensaje', exception.toString());
        res.redirect('/carreras');
    }
});

router.post('/:id/editar', function (req, res, next) {
    try {
        const data = {
            numero: req.body.numero,
            descripcion: req.body.descripcion,
            fecha: `${req.body.fecha} ${req.body.horaInicio}`,
            distancia: req.body.distancia,
        };
        const carrera = new Carrera(data);
        carrera.validate()
            .then(() => {
                Carrera.findOneAndUpdate({_id: req.params.id}, data)
                    .then(p => {
                        console.log(p);
                        req.flash('mensaje', `La carrera con id ${p._id} ha sido actualizada`);
                        res.redirect('/carreras');
                    })
                    .catch(err => {
                        console.error(err);
                        req.flash('mensaje', err);
                        res.redirect('/carreras/crear');
                    })
            })
            .catch(err => {
                console.error(err);
                req.flash('mensaje', err.message);
                res.redirect('/carreras/crear');
            })
    } catch (exception) {
        console.exception(exception);
        req.flash('mensaje', exception);
        res.redirect('/carreras/crear');
    }
});

router.post('/:id/participantes/calcularTiempos', function (req, res, next) {

    if (Object.keys(req.files).length === 0 || (req.files.tiempos === void 0)) {
        return res.status(400).send('No se ha subido ningun fichero');
    }

    const fichero = req.files.tiempos;
    if (fichero.data.length === 0) {
        return res.status(400).send('El fichero está vacio');
    }

    let resultados = [];
    const regex = /^([a-fA-F\d]{16}) (\d{6})$/gm;
    let m;
    while ((m = regex.exec(fichero.data.toString())) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        resultados = [...resultados, {codigo: m[1], tiempo: m[2]}];
    }

    if (resultados.length === 0) {
        return res.status(400).send('No se han encontrado registros validos en el fichero.');
    }



    resultados.sort((a, b) => a.tiempo - b.tiempo);
    try {
        Carrera.findById(req.params.id)
            .populate({
                path: 'participantes',
                select: ['dorsal', 'nombre', 'apellidos', 'dni', 'club', 'tiempo', 'posicion'],
                populate: {path: 'dorsal'}
            })
            .then(carrera => {
                //console.log(carrera);

                let horaInicio = new Date();
                horaInicio.setHours(carrera.fecha.getHours());
                horaInicio.setMinutes(carrera.fecha.getMinutes());
                horaInicio.setSeconds(carrera.fecha.getSeconds());

                carrera.participantes
                    .forEach(p => {
                        let posicion = resultados.findIndex(linea => linea.codigo === p.dorsal.codigoChip) + 1;
                        if (posicion === 0) {
                            console.log(`No se ha encontrado registro de tiempo para el participante con id: ${p._id}`);
                            return;
                        }
                        let horaLlegada = resultados.find(linea => linea.codigo === p.dorsal.codigoChip).tiempo;
                        let tiempoEnSegundos = moment(horaLlegada, "HHmmss").diff(moment(horaInicio), "seconds");

                        p.tiempo = new Date(1970, 1, 1, 0, 0, tiempoEnSegundos);
                        p.posicion = posicion;
                        p.save()
                            .then(d => {
                                //console.log(d);
                            })
                            .catch(err => {
                                console.error(err);
                            })
                    });

                let registro = {
                    carreraNumParticipantes: carrera.participantes.length,
                    lineasFichero: resultados.length,
                    errores: {
                        codigosSinParticipante: resultados.filter(r => !carrera.participantes.some(p => p.dorsal.codigoChip === r.codigo)),
                        participantesSinCodigo: carrera.participantes.filter(p => !resultados.some(r => r.codigo === p.dorsal.codigoChip)),
                    }

                };
                req.flash('registro', registro);
                res.redirect(`/carreras/${req.params.id}/participantes`);
            })
            .catch(err => {
                console.error(err);
                //res.render('carreras/listado', {mensaje: err});
            })

    } catch (e) {
        console.exception(e);
        res.render('carreras/listado', {mensaje: e});
    }

});

router.get('/:id/participantes/limpiarTiempos', function (req, res, next) {
    try {
        Carrera.findById(req.params.id)
            .populate({
                path: 'participantes',
                select: ['dorsal', 'nombre', 'apellidos', 'dni', 'club', 'tiempo', 'posicion'],
            })
            .then(carrera => {

                Participante.update(
                    {_id: {"$in": carrera.participantes.map(p => p._id)}},
                    {$set: {tiempo: null, posicion: null}},
                    {multi: true})
                    .then(participantes => {
                        //console.log(participantes);
                    })
                    .catch(err => {
                        console.error(err);
                        req.flash('mensaje', err);
                    })
            })

            .catch(err => {
                console.error(err);
                req.flash('mensaje', err);
                res.redirect(`/carreras/${req.params.id}/participantes`);
            })
            .finally(() => {
                req.flash('mensaje', `Los tiempos y posiciones de los participantes han sido reseteados`);
                res.redirect(`/carreras/${req.params.id}/participantes`);
            });
    } catch (exception) {
        console.log(exception);
        req.flash('mensaje', exception.toString());
        res.redirect(`/carreras/${req.params.id}/participantes`);
    }
});

router.get('/:id/entregarDorsales', function (req, res, next) {
    try {
        Carrera.findById(req.params.id)
            .populate({
                path: 'participantes',
                select: ['dorsal', 'nombre', 'apellidos', 'dni', 'club', 'tiempo', 'posicion'],
                populate: {
                    path: 'dorsal'
                }
            })
            .then(carrera => {
                console.log(carrera);

                carrera.participantes
                    .forEach(p => {
                        Dorsal.findByIdAndUpdate(p.dorsal._id, {$set: {codigoChip: new Buffer(p.dorsal.numero.toString()).toString('hex').toUpperCase().padStart(24, '0')}})
                            .then(d => {
                                console.log(d);
                            })
                            .catch(err => {
                                console.error(err);
                            })
                    })
            })
            .catch(err => {
                console.error(err);
            });
    } catch (exception) {
        console.log(exception);
        req.flash('mensaje', exception.toString());
        res.redirect('/carreras');
    }
});

router.get('/:id/eliminar', function (req, res, next) {
    try {
        Carrera.findByIdAndDelete(req.params.id)
            .then(carrera => {
                console.log(carrera);
                Participante.deleteMany({_id: {$in: carrera.participantes}})
                    .then(participantes => {
                        console.log(participantes);
                    }).catch(err => {
                        console.error(err);
                    });
                Participante.deleteMany({_id: {$in: carrera.participantes}})
                    .then(participantes => {
                        console.log(participantes);
                    }).catch(err => {
                    console.error(err);
                });

                req.flash('mensaje', `La carrera y todos sus participantes han sido eliminados`);
                res.redirect('/carreras');
            })
            .catch(err => {
                console.log(err);
                req.flash('mensaje', err.toString());
                res.redirect('/carreras');
            });
    } catch (exception) {
        console.log(exception);
        req.flash('mensaje', exception.toString());
        res.redirect('/carreras');
    }

});

module.exports = router;
