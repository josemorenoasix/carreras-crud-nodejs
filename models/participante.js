const mongoose = require('mongoose');

const ParticipanteSchema = mongoose.Schema ({
    dorsal: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Dorsal'
    },
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    apellidos: {
        type: String,
        required: true,
        trim: true
    },
    dni: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    club: {
        type: String,
        default: "",
        trim: true
    },
    carrera: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Carrera'
    },
    posicion: {
        type: Number,
    },
    tiempo: {
        type: Date,
    },
}, {
    timestamps: true
});

// Export the model
module.exports = mongoose.model('Participante', ParticipanteSchema);