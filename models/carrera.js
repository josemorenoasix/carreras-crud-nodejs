const mongoose = require('mongoose');

const CarreraSchema = mongoose.Schema ({
    numero: {
        type: Number,
        required: true,
        unique: true
    },
    descripcion: {
        type: String,
        required: true,
        trim: true
    },
    fecha: {
        type: Date,
        required: true
    },
    distancia: {
        type: Number,
        required: true
    },
    participantes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Participante'
    }]

}, {
    timestamps: true
});



// Export the model
module.exports = mongoose.model('Carrera', CarreraSchema);